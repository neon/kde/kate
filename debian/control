Source: kate
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Modestas Vainius <modax@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               kf6-extra-cmake-modules,
               kf6-kconfig-dev,
               kf6-kcrash-dev,
               kf6-kdbusaddons-dev,
               kf6-kcolorscheme-dev,
               kf6-kdoctools-dev,
               kf6-kguiaddons-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-kitemmodels-dev,
               kf6-kjobwidgets-dev,
               kf6-kio-dev,
               kf6-knewstuff-dev,
               kf6-kparts-dev,
               kf6-kpty-dev,
               kf6-syntax-highlighting-dev,
               kf6-ktexteditor-dev,
               kf6-threadweaver-dev,
               kf6-kwallet-dev,
               kf6-kwindowsystem-dev,
               kf6-kuserfeedback-dev,
               kf6-kxmlgui-dev,
               libplasma-dev,
               pkgconf,
               pkg-kde-tools-neon,
               plasma-activities-dev,
               qt6-base-dev
Standards-Version: 4.6.2
Homepage: http://kate-editor.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kate
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kate.git

Package: kate
Section: editors
Architecture: any
Depends: kate6-data (>= ${source:Version}),
         kf6-ktexteditor,
         libplasma6,
         kf6-kdeclarative,
         qt6-declarative,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: kf6-sonnet
Suggests: darcs,
          exuberant-ctags,
          git,
          khelpcenter,
          konsole-kpart,
          mercurial,
          subversion
Description: powerful text editor
 Kate is a powerful text editor that can open multiple files simultaneously.
 .
 With a built-in terminal, syntax highlighting, and tabbed sidebar, it performs
 as a lightweight but capable development environment.  Kate's many tools,
 plugins, and scripts make it highly customizable.
 .
 Kate's features include:
 .
  * Multiple saved sessions, each with numerous files
  * Scriptable syntax highlighting, indentation, and code-folding
  * Configurable templates and text snippets
  * Symbol viewers for C, C++, and Python
  * XML completion and validation

Package: kate6-data
Architecture: all
Depends: ${misc:Depends}
Replaces: kate5-data
Breaks: kate5-data
Description: shared data files for Kate text editor
 This package contains the architecture-independent shared data files needed
 for Kate editor

Package: kwrite
Section: editors
Architecture: any
Depends: kf6-ktexteditor, ${misc:Depends}, ${shlibs:Depends}
Breaks: kate5-data
Replaces: kate5-data
Description: simple text editor
 KWrite is a simple text editor built on the KDE Platform. It uses the Kate
 editor component, so it supports powerful features such as flexible syntax
 highlighting, automatic indentation, and numerous other text tools.
